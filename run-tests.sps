;; run all tests - customised for Gauche and Sagittarius

(import (scheme base)
        (srfi 152))

(cond-expand
  (gauche
   (import (gauche process)
           (file util))

   (define (run-scheme filename)
     (do-process `(gosh "-I" "." ,filename)))

   (define (get-all-files directory)
     (reverse (directory-fold directory cons '()))))

  (sagittarius
    (import (sagittarius process)
            (util file))

    (define (run-scheme filename)
      (run "sash" "-L" "." "-L" "sagittarius" filename))

    (define get-all-files find-files))

  (else
    (error "Unsupported Scheme implementation")))

;; run each test file, which is identified by name *-test.sps
(for-each
  (lambda (folder)
    (for-each
      (lambda (filename)
        (when (string-suffix? "-test.sps" filename)
          (run-scheme filename)))
      (get-all-files folder)))
  (list "nltk-tests" "pfds-tests" "rebottled-tests" "robin-tests" "slib-tests" "srfi-tests"))

