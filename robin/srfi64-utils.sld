;; SRFI 64 helper functions
;; written by Peter Lane, 2017

(define-library
  (robin srfi64-utils)
  (export test-all-equal
          test-compare
          test-no-error)
  (import (scheme base)
          (scheme case-lambda))

  ; Sagittarius exports a conflicting function!
  (cond-expand
    (sagittarius
     (import (except (srfi 64) test-compare)))
    (else
      (import (srfi 64))))

  (begin

    ;; Uses test-equal on all pairs in given association list
    (define (test-all-equal fn pairs)
      (for-each (lambda (pair)
                  (test-equal (cdr pair) (fn (car pair))))
                pairs))

    ;; Test if two given items satisfy the given comparison procedure
    (define (test-compare proc l1 l2)
      (test-assert (proc l1 l2)))

    ;; Test fails if an error is raised
    (define-syntax test-no-error
      (syntax-rules ()
                    ((test-no-error code)
                     (guard (err
                              (else (test-assert #f)))
                            code
                            (test-assert #t)))))

    ))

