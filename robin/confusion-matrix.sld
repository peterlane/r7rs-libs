
(define-library
  (robin confusion-matrix)
  (export make-confusion-matrix
          confusion-matrix?
          confusion-matrix-add
          confusion-matrix-count
          confusion-matrix-display
          confusion-matrix-labels
          confusion-matrix-total
          ;
          cohen-kappa
          false-negative
          false-positive
          false-rate
          f-measure
          geometric-mean
          matthews-correlation
          overall-accuracy
          precision
          prevalence
          recall
          sensitivity
          specificity
          true-negative
          true-positive
          true-rate
          )
  (import (scheme base)
          (scheme case-lambda)
          (scheme inexact)
          (scheme list)
          (scheme write))

  (begin

    ;; -- utility functions

    ;; create initial association list using cartesian product of labels
    (define (initial-counts labels)
      (apply append
             (map (lambda (predicted)
                    (map (lambda (observed)
                           (cons (list predicted observed) 0))
                         labels))
                  labels)))

    ;; returns if y is zero
    (define (safe/ x y)
      (if (zero? y)
        y
        (/ x y)))

    ;; create a record type for a confusion-matrix
    (define-record-type <confusion-matrix>
      (%make-confusion-matrix labels counts)
      confusion-matrix?
      (labels confusion-matrix-labels)
      (counts confusion-matrix-counts))

    ;; create an instance of a confusion matrix
    ;; - uses optional labels
    (define make-confusion-matrix
      (case-lambda 
        (()
         (make-confusion-matrix (list 'positive 'negative)))
        ((labels)
         (if (< (length labels) 2)
           (error "There must be at least 2 labels")
           (%make-confusion-matrix labels (initial-counts labels))))))

    ;; internal function, retrieves count pair, or error if pair not known
    (define (retrieve-count cm predicted observed)
      (or (assoc (list predicted observed)
                 (confusion-matrix-counts cm))
          (error "Unknown entry in confusion matrix")))

    ;; add given count to (predicted observed) pair
    (define confusion-matrix-add
      (case-lambda 
        ((cm predicted observed)
         (confusion-matrix-add cm predicted observed 1))
        ((cm predicted observed total)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((stored (retrieve-count cm predicted observed)))
           (set-cdr! stored (+ (cdr stored) total))))))

    ;; return count for (predicted observed) pair
    (define (confusion-matrix-count cm predicted observed)
      (unless (confusion-matrix? cm)
        (error "Requires a confusion matrix instance"))
      (cdr (retrieve-count cm predicted observed)))

    ;; displays confusion matrix to current-output
    (define (confusion-matrix-display cm)
      (define (as-string val)
        (cond ((string? val)
               val)
              ((number? val)
               (number->string val))
              ((symbol? val)
               (symbol->string val))
              (else
                (error "Unrecognised label type"))))
      (define (longest-label)
        (let ((longest 4)) ; minimum size
          (for-each (lambda (label)
                      (let ((len (string-length (as-string label))))
                        (when (> len longest)
                          (set! longest len))))
                    (confusion-matrix-labels cm))
          longest))
      (define (pad val size)
        (let ((str-val (as-string val)))
          (if (>= (string-length str-val) size)
            str-val
            (string-append (make-string (- size (string-length str-val)) #\space)
                           str-val))))
      ;
      (unless (confusion-matrix? cm)
        (error "Requires a confusion matrix instance"))
      (let ((column-width (longest-label)))
        ; initial line
        (display "Observed") (newline)
        ; labels line
        (for-each (lambda (label)
                    (display " ") (display (pad label column-width)))
                  (confusion-matrix-labels cm))
        (display " | Predicted") (newline)
        ; horizontal line
        (display (make-string (* (length (confusion-matrix-labels cm))
                                 (+ 1 column-width) )
                              #\-)) 
        (display "-+-----------")
        (newline)
        ; display all rows of values
        (for-each (lambda (predicted)
                    ; row of values
                    (for-each (lambda (observed)
                                (display " ") (display 
                                                (pad (confusion-matrix-count cm predicted observed) column-width)))
                              (confusion-matrix-labels cm))
                    (display " | ") (display predicted) (newline))
                  (confusion-matrix-labels cm))))

    ;; Returns the total number of instances referenced in the confusion matrix
    (define (confusion-matrix-total cm)
      (unless (confusion-matrix? cm)
        (error "Requires a confusion matrix instance"))
      (fold + 0 (map cdr (confusion-matrix-counts cm))))

    ;; ----------------------------------------------------------------------
    ;; Quantitative Measures

    ;; Cohen's Kappa statistic compares observed accuracy with an expected accuracy
    (define cohen-kappa
      (case-lambda 
        ((cm)
         (cohen-kappa cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let* ((tp (true-positive cm for-label))
                (fn (false-negative cm for-label))
                (fp (false-positive cm for-label))
                (tn (true-negative cm for-label))
                (total (+ tp fn fp tn))
                (total-accuracy (safe/ (+ tp tn) total))
                (random-accuracy (safe/ (+ (* (+ tn fp) (+ tn fn))
                                           (* (+ fn tp) (+ fp tp)))
                                        (* total total))))
           (safe/ (- total-accuracy random-accuracy)
                  (- 1 random-accuracy))))))

    ;; Returns the number of instances of the given label which are incorrectly observed
    (define false-negative 
      (case-lambda 
        ((cm)
         (false-negative cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (fold (lambda (observed total)
                 (+ total
                    (if (equal? for-label observed)
                      0
                      (confusion-matrix-count cm for-label observed))))
               0
               (confusion-matrix-labels cm)))))

    ;; Returns the number of instances incorrectly observed of the given label
    (define false-positive 
      (case-lambda 
        ((cm)
         (false-positive cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (fold (lambda (predicted total)
                 (+ total
                    (if (equal? predicted for-label)
                      0
                      (confusion-matrix-count cm predicted for-label))))
               0 
               (confusion-matrix-labels cm)))))

    ;; Proportion of instances of label incorrectly observed out of all
    ;; instances not originally of that label
    (define false-rate 
      (case-lambda 
        ((cm)
         (false-rate cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((fp (false-positive cm for-label))
               (tn (true-negative cm for-label)))
           (safe/ fp (+ fp tn))))))

    ;; F-measure is the harmonic mean of the precision and recall for the given label
    (define f-measure
      (case-lambda 
        ((cm)
         (f-measure cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((precision (precision cm for-label))
               (recall (recall cm for-label)))
           (safe/ (* 2 precision recall)
                  (+ precision recall))))))

    ;; Geometric-mean is the nth root of product of true-rate for each label
    (define (geometric-mean cm)
      (unless (confusion-matrix? cm)
        (error "Requires a confusion matrix instance"))
      (expt
        (fold (lambda (label product)
                (* product (true-rate cm label)))
              1.0
              (confusion-matrix-labels cm))
        (/ 1 (length (confusion-matrix-labels cm)))))

    ;; Matthews-Correlation is a measure of the quality of binary classifications
    (define matthews-correlation
      (case-lambda 
        ((cm)
         (matthews-correlation cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((tp (true-positive cm for-label))
               (fn (false-negative cm for-label))
               (fp (false-positive cm for-label))
               (tn (true-negative cm for-label)))
           (safe/ (- (* tp tn) (* fp fn))
                  (sqrt (* (+ tp fp) (+ tp fn) (+ tn fp) (+ tn fn))))))))

    ;; Overall-accuracy is proportion of instances which are correctly labelled
    (define (overall-accuracy cm)
      (unless (confusion-matrix? cm)
        (error "Requires a confusion matrix instance"))
      (safe/ (fold + 0
                   (map (lambda (label) (confusion-matrix-count cm label label))
                        (confusion-matrix-labels cm)))
             (confusion-matrix-total cm)))

    ;; Precision is proportion of instances of given label which are correct
    (define precision
      (case-lambda 
        ((cm)
         (precision cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((tp (true-positive cm for-label))
               (fp (false-positive cm for-label)))
           (safe/ tp (+ tp fp))))))

    ;; Prevalence is the proportion of instances observed of given label, out
    ;; of total
    (define prevalence
      (case-lambda 
        ((cm)
         (prevalence cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((tp (true-positive cm for-label))
               (fn (false-negative cm for-label)))
           (safe/ (+ tp fn) (confusion-matrix-total cm))))))

    ;; Specificity is 1 - false-rate, for a given label
    (define specificity
      (case-lambda 
        ((cm)
         (specificity cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (- 1 (false-rate cm for-label)))))

    ;; Returns the number of instances NOT of the given label which are correctly observed
    (define true-negative
      (case-lambda 
        ((cm)
         (true-negative cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (fold (lambda (predicted total)
                 (+ total
                    (if (equal? predicted for-label)
                      0
                      (confusion-matrix-count cm predicted predicted))))
               0 
               (confusion-matrix-labels cm)))))

    ;; Returns the number of instances of the given label correctly observed
    (define true-positive
      (case-lambda 
        ((cm)
         (true-positive cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (confusion-matrix-count cm for-label for-label))))

    ;; Proportion of instances of given label which are correctly observed
    (define true-rate
      (case-lambda 
        ((cm)
         (true-rate cm (car (confusion-matrix-labels cm))))
        ((cm for-label)
         (unless (confusion-matrix? cm)
           (error "Requires a confusion matrix instance"))
         (let ((tp (true-positive cm for-label))
               (fn (false-negative cm for-label)))
           (safe/ tp (+ tp fn))))))

    ;; Recall is equal to the true rate, for a given label
    (define recall true-rate)

    ;; Sensitivity is another name for the true positive rate (recall)
    (define sensitivity true-rate)

    )) ; end of library

