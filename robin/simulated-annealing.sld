;; Simulated Annealing

(define-library
  (robin simulated-annealing)
  (export simulated-annealing)
  (import (scheme base)
          (scheme case-lambda)
          (scheme inexact)
          (srfi 27))

  (begin

    (random-source-randomize! default-random-source)

    ;; make-instance: function of 0/1 arguments which returns a new, random instance, or a neighbour of given instance
    ;; evaluate-instance: function of 1 argument, an instance, returning a positive number indicating quality of solution, with 0 being perfect
    ;; num-iterations: number of cycles to perform
    (define simulated-annealing
      (case-lambda
        ((make-instance evaluate-instance num-iterations)
         (simulated-annealing make-instance evaluate-instance num-iterations
                              (lambda (n) #f)))
        ((make-instance evaluate-instance num-iterations can-stop?)
         (unless (and (number? num-iterations)
                      (positive? num-iterations)
                      (integer? num-iterations))
           (error "simulated-annealing: num-iterations must be a positive integer, not" num-iterations))
         (let* ((initial-instance (make-instance))
                (initial-performance (evaluate-instance initial-instance)))
           (unless (and (number? initial-performance) 
                        (not (negative? initial-performance)))
             (error "simulated-annealing: evaluate-instance must return a non-negative value, not" initial-performance))
           (let loop ((n 1)
                      (current-instance initial-instance)
                      (current-performance initial-performance))
             (if (or (= n num-iterations)
                     (can-stop? current-performance))
               current-instance
               (let* ((T (- 1 (/ n num-iterations)))
                      (new-instance (make-instance current-instance))
                      (new-performance (evaluate-instance new-instance)))
                 (unless (and (number? new-performance) 
                              (not (negative? new-performance)))
                   (error "simulated-annealing: evaluate-instance must return a non-negative value, not" new-performance))
                 (if (>= (criterion current-performance new-performance T)
                         (random-real))
                   (loop (+ 1 n) 
                         new-instance
                         new-performance)
                   (loop (+ 1 n)
                         current-instance
                         current-performance)))))))))

    ;; the Metropolis acceptance criterion
    ;; es - current instance evaluation
    ;; en - new/candidate instance evaluation
    ;; T - temperature value
    (define (criterion es en T)
      (if (< en es)
        1.0
        (exp (/ (- es en) T))))

    )) ; end of library

