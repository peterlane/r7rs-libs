;; Text library for R7RS Scheme

;; Written by Peter Lane, 2017-22

(define-library
  (robin text)
  (export word-wrap
          words->with-commas
          string->n-grams
          ; metrics and similarity measures
          soundex ; straight from (slib soundex)
          daitch-mokotoff-soundex
          russell-soundex
          metaphone 
          hamming-distance
          levenshtein-distance
          optimal-string-alignment-distance
          ;
          sorenson-dice-similarity
          porter-stem
          ;
          generate-password
          )
  (import (scheme base)
          (scheme case-lambda)
          (scheme char)
          (scheme inexact)
          (scheme list)
          (scheme sort)
          (scheme write)
          (rebottled pregexp)
          (only (robin statistics) random-pick sorenson-dice-index)
          (slib soundex))

  (cond-expand
    ((library (srfi 152))
     (import (srfi 152)))
    ((library (srfi 13))
     (import (except (srfi 13) string-map string-for-each)))
    (else
      "Scheme implementation does not support SRFI 152 or 13"))

  (begin

    ;; word wrap, using greedy algorithm with minimum lines
    ;; returns a list of lines
    (define (word-wrap str width)
      (let loop ((words (pregexp-split " +" str))
                 (line-length 0)
                 (line "")
                 (lines '()))
        (cond ((null? words)
               (reverse (cons line lines)))
              ((> (+ line-length (string-length (car words)))
                  width)
               (if (zero? (string-length line))
                 (loop (cdr words) ; case where word exceeds line length
                       0
                       ""
                       (cons (car words) lines))
                 (loop words ; word must go to next line, so finish current line
                       0
                       ""
                       (cons line lines))))
              (else
                (loop (cdr words) ; else, add word to current line
                      (+ 1 line-length (string-length (car words)))
                      (if (zero? (string-length line))
                        (car words)
                        (string-join (list line (car words)) " "))
                      lines)))))

    ;; given a list of strings, each string representing a word, 
    ;; return a string with each word separated by commas, 
    ;; but last separated by 'and'.
    ;; Optional flag is used to include the comma before 'and', if required.
    (define words->with-commas 
      (case-lambda
        ((words) ; default to no optional comma
         (words->with-commas words #f))
        ((words add-comma?)
         (parameterize ((current-output-port (open-output-string)))
                       (do ((rem words (cdr rem)))
                         ((null? rem) )
                         (display (car rem))
                         (cond ((= 1 (length rem)) )
                               ((= 2 (length rem))
                                (when (and add-comma? 
                                           (> (length words) 2))
                                  (display ","))
                                (display " and "))
                               (else
                                 (display ", "))))
                       (get-output-string (current-output-port))))))

    ;; Convert a string to a list of letter n-grams
    (define (string->n-grams str n)
      (let ((len (string-length str)))
        (cond ((<= n 0)
               (error "string->n-grams needs n >= 1"))
              ((> n len)
               (list str))
              (else
                (do ((i 0 (+ 1 i))
                     (res '() (cons (string-copy str i (+ i n)) res)))
                  ((> (+ i n) len) (reverse res)))))))

    ;; Phonetic-based metrics

    ;; daitch-mokotoff-soundex
    ;; Converts given word into a coded string of length 6
    ;; Table from: http://www.jewishgen.org/InfoFiles/soundex.html
    (define daitch-mokotoff-soundex
      (let ((table 
              (list-sort
                (lambda (a b) (> (string-length (car a)) (string-length (car b))))
                '(("AI" "0" "1" "-") ; (letter(s) -> at-start  before-vowel  elsewhere)
                  ("AJ" "0" "1" "-")
                  ("AY" "0" "1" "-")
                  ("AU" "0" "7" "-")
                  ("A" "0" "-" "-")
                  ("Ą" "-" "-" ("6" "-")) ; Polish a-ogonek
                  ("B" "7" "7" "7")
                  ("C" ("4" "5") ("4" "5") ("4" "5")) 
                  ("CH" ("5" "4") ("5" "4") ("5" "4")) 
                  ("CK" ("5" "45") ("5" "45") ("5" "45"))
                  ("CHS" "5" "54" "54")
                  ("CK" "4" "4" "4")
                  ("CS" "4" "4" "4")
                  ("CSZ" "4" "4" "4")
                  ("CZS" "4" "4" "4")
                  ("CZ" "4" "4" "4")
                  ("DRZ" "4" "4" "4")
                  ("DRS" "4" "4" "4")
                  ("DS" "4" "4" "4")
                  ("DSH" "4" "4" "4")
                  ("DSZ" "4" "4" "4")
                  ("DZ" "4" "4" "4")
                  ("DZH" "4" "4" "4")
                  ("DZS" "4" "4" "4")
                  ("D" "3" "3" "3")
                  ("DT" "3" "3" "3")
                  ("Ę", "-" "-" ("6" "-")) ; Polish e-ogonek
                  ("EI" "0" "1" "-") 
                  ("EJ" "0" "1" "-")
                  ("EY" "0" "1" "-")
                  ("EU" "1" "1" "-")
                  ("E" "0" "-" "-")
                  ("FB" "7" "7" "7")
                  ("F" "7" "7" "7")
                  ("G" "5" "5" "5")
                  ("H" "5" "5" "-")
                  ("IA" "1" "-" "-")
                  ("IE" "1" "-" "-")
                  ("IO" "1" "-" "-")
                  ("IU" "1" "-" "-")
                  ("I" "0" "-" "-")
                  ("J" ("1" "4") ("-" "4") ("-" "4"))
                  ("KS" "5" "54" "54")
                  ("KH" "5" "5" "5")
                  ("K" "5" "5" "5")
                  ("L" "8" "8" "8")
                  ("MN" "-" "66" "66")
                  ("M" "6" "6" "6")
                  ("NM" "-" "66" "66")
                  ("N" "6" "6" "6")
                  ("OI" "0" "1" "-")
                  ("OJ" "0" "1" "-")
                  ("OY" "0" "1" "-")
                  ("O" "0" "-" "-")
                  ("P" "7" "7" "7")
                  ("PF" "7" "7" "7")
                  ("PH" "7" "7" "7")
                  ("Q" "5" "5" "5")
                  ("R" "9" "9" "9")
                  ("RS" ("4" "94") ("4" "94") ("4" "94"))
                  ("RZ" ("4" "94") ("4" "94") ("4" "94")) 
                  ("SCHTSCH" "2" "4" "4")
                  ("SCHTSH" "2" "4" "4")
                  ("SCHTCH" "2" "4" "4")
                  ("SCH" "4" "4" "4")
                  ("SHTCH" "2" "4" "4")
                  ("SHCH" "2" "4" "4")
                  ("SHTSH" "2" "4" "4")
                  ("SHT" "2" "43" "43")
                  ("SH" "4" "4" "4")
                  ("STCH" "2" "4" "4")
                  ("STSCH" "2" "4" "4")
                  ("SC" "2" "4" "4")
                  ("STRZ" "2" "4" "4")
                  ("STRS" "2" "4" "4")
                  ("STSH" "2" "4" "4")
                  ("ST" "2" "43" "43")
                  ("SZCZ" "2" "4" "4")
                  ("SZCS" "2" "4" "4")
                  ("SZT" "2" "43" "43")
                  ("SHD" "2" "43" "43")
                  ("SZD" "2" "43" "43")
                  ("SD" "2" "43" "43")
                  ("SZ" "4" "4" "4")
                  ("S" "4" "4" "4")
                  ("Ţ" ("3" "4") ("3" "4") ("3" "4")) ; Romanian t-cedilla 
                  ("TCH" "4" "4" "4")
                  ("TTCH" "4" "4" "4")
                  ("TTSCH" "4" "4" "4")
                  ("TH" "3" "3" "3")
                  ("TRZ" "4" "4" "4")
                  ("TRS" "4" "4" "4")
                  ("TSCH" "4" "4" "4")
                  ("TSH" "4" "4" "4")
                  ("TS" "4" "4" "4")
                  ("TTS" "4" "4" "4")
                  ("TTSZ" "4" "4" "4")
                  ("TC" "4" "4" "4")
                  ("TZ" "4" "4" "4")
                  ("TTZ" "4" "4" "4")
                  ("TZS" "4" "4" "4")
                  ("TSZ" "4" "4" "4")
                  ("T" "3" "3" "3")
                  ("UI" "0" "1" "-")
                  ("UJ" "0" "1" "-")
                  ("UY" "0" "1" "-")
                  ("U" "0" "-" "-")
                  ("UE" "0" "-" "-")
                  ("V" "7" "7" "7")
                  ("W" "7" "7" "7")
                  ("X" "5" "54" "54")
                  ("Y" "1" "-" "-")
                  ("ZDZ" "2" "4" "4")
                  ("ZDZH" "2" "4" "4")
                  ("ZHDZH" "2" "4" "4")
                  ("ZD" "2" "43" "43")
                  ("ZHD" "2" "43" "43")
                  ("ZH" "4" "4" "4")
                  ("ZS" "4" "4" "4")
                  ("ZSCH" "4" "4" "4")
                  ("ZSH" "4" "4" "4")
                  ("Z" "4" "4" "4")))))
        (define (longest-match str) 
          (let loop ((words table))
            (cond ((null? words)
                   #f)
                  ((string-prefix? (caar words) str)
                   (caar words))
                  (else
                    (loop (cdr words))))))
        (define (match-result codes word posn next-posn)
          (cond ((zero? posn)
                 (list-ref codes 0))
                ((and (< (+ 1 next-posn) (string-length word))
                      (member (string-ref word (+ 1 next-posn))
                              (string->list "AEIOU")
                              char=?))
                 (list-ref codes 1))
                (else
                  (list-ref codes 2))))
        (define (valid-match? codes word posn next-posn)
          (let ((m (match-result codes word posn next-posn)))
            (or (list? m)
                (not (string=? "-" m)))))
        (define (generate-results result) ; some of the terms may be lists, so make multiple terms for these
          (let ((first-list (list-index list? result)))
            (if (number? first-list)
              (let ((list-1 (list-copy result))
                    (list-2 (list-copy result))
                    (mix (list-ref result first-list)))
                (append
                  (generate-results (append (take list-1 first-list)
                                            (list (car mix))
                                            (drop list-1 (+ 1 first-list))))
                  (generate-results (append (take list-2 first-list)
                                            (list (cadr mix))
                                            (drop list-2 (+ 1 first-list))))))              
              (if (< (length result) 6)
                (list (apply string-append (append result (make-list (- 6 (length result)) "0"))))
                (list (apply string-append (take result 6)))))))
        ;
        (case-lambda
          ((input-word)
           (daitch-mokotoff-soundex input-word 'single))
          ((input-word option)
           (let ((word (string-upcase input-word)))
             (let loop ((posn 0)
                        (match (longest-match word))
                        (result '()))
               (cond ((>= posn (string-length word))
                      (let ((codes (generate-results (reverse result))))
                        (if (eq? option 'single)
                          (car codes)
                          codes)))
                     ((string? match)
                      (let ((codes (cdr (assoc match table string=?))))
                        (if (valid-match? codes word posn (+ posn (string-length match)))
                          (loop (+ posn (string-length match))
                                (longest-match (string-copy word (+ posn (string-length match))))
                                (cons (match-result codes word posn (+ posn (string-length match))) result))
                          (loop (+ posn (string-length match))
                                (longest-match (string-copy word (+ posn (string-length match))))
                                result))))
                     (else (loop (+ 1 posn)
                                 (longest-match (string-copy word (+ 1 posn)))
                                 result)))))))))

    (define russell-soundex soundex) ; from (slib soundex)

    ; metaphone: there is no consistent description of this algorithm
    ; these rules are applied from the description at http://aspell.net/metaphone/metaphone-kuhn.txt
    (define (metaphone word)
      ; remove duplicate letters in target alphabet (but leave 'c' alone)
      (define (remove-duplicates-but-c str) 
        (define target-group '(#\b #\f #\h #\j #\k #\l #\m #\n #\p #\r #\s #\t #\w #\x #\y))
        (define (update-res i res)
          (if (and (memv (string-ref str i) target-group)
                   (< i (- (string-length str) 1))
                   (char=? (string-ref str i) (string-ref str (+ i 1))))
            res
            (cons (string-ref str i) res)))
        (do ((i 0 (+ i 1))
             (res '() (update-res i res)))
          ((= i (string-length str)) (list->string (reverse res)))))
      ; adapt initial letters:
      ;   "ae-", "gn", "kn-", "pn-", "wr-" drop first letter
      ;   "x" -> "s"
      ;   "wh" -> "w"
      (define (adapt-initial-letters str)
        (cond ((member (string-copy str 0 2) '("ae" "gn" "kn" "pn" "wr") string=?)
               (string-copy str 1))
              ((char=? #\x (string-ref str 0))
               (string-append "s" (string-copy str 1)))
              ((string=? "wh" (string-copy str 0 2))
               (string-append "w" (string-copy str 2)))
              (else
                str)))
      ; remove b in mb$
      (define (remove-b str)
        (if (string-suffix? "mb" str)
          (string-drop-right str 1) ; replaces mb$ with m$
          str))
      ; helper functions
      (define (vowel? c) (memq c '(#\a #\e #\i #\o #\u)))
      (define (one-of? grp) (lambda (c) (memq c grp)))
      (define (peek? rem c)
        (and (not (null? rem))
             (not (null? (cdr rem)))
             (if (char? c) ; if not a char, assume it's a proc
               (char=? c (cadr rem))
               (c (cadr rem)))))
      (define (peek-io/a? rem) ; is top char followed by io or ia
        (and (peek? rem #\i)
             (or (peek? (cdr rem) #\o)
                 (peek? (cdr rem) #\a))))
      ;
      (let* ((lword (string-filter char-alphabetic? (string-downcase word)))
             (dedupped (remove-duplicates-but-c lword))
             (initials (adapt-initial-letters dedupped))
             (no-mb (remove-b initials)))
        (let loop ((rem (string->list no-mb))
                   (last-done #f)             ; last actual character processed
                   (res '()))
          (if (null? rem)
            (string-upcase (list->string (reverse res)))
            (case (car rem)
              ((#\a #\e #\i #\o #\u) ; keep vowel only at front
               (if (null? res) 
                 (loop (cdr rem) (car rem) (cons (car rem) res))
                 (loop (cdr rem) last-done res)))
              ((#\b #\f #\j #\l #\m #\n #\r) ; retained
               (loop (cdr rem) (car rem) (cons (car rem) res)))
              ((#\c)
               (cond ((peek? rem #\h)
                      (loop (cdr rem)
                            (car rem)
                            (cons (if (eqv? last-done #\s) #\k #\x)
                                  res)))
                     ((and (peek? rem #\i)
                           (peek? (cdr rem) #\a))
                      (loop (cdr rem) (car rem) (cons #\x res)))
                     ((peek? rem (one-of? '(#\e #\i #\y)))
                      (loop (cdr rem)
                            (car rem)
                            (if (eqv? last-done #\s)
                              res ; silent if -sci- -sce- -scy-
                              (cons #\s res))))
                     (else
                       (loop (cdr rem) (car rem) (cons #\k res)))))
              ((#\d)
               (loop (cdr rem)
                     (car rem)
                     (cons 
                       (if (and (peek? rem #\g)
                                (peek? (cdr rem) (one-of? '(#\e #\i #\y))))
                         #\j
                         #\t)
                       res)))
              ((#\g)
               (cond ((peek? rem #\g) ; duplicate g
                      (loop (cdr rem) (car rem) res))
                     ((or (and (peek? rem #\h)
                               (> (length rem) 1) ; not at end
                               (not (peek? (cdr rem) vowel?))) ; not before a vowel
                          (peek? rem #\n)
                          (and (eqv? last-done #\d)
                               (peek? rem (one-of? '(#\e #\i #\y)))))
                      (loop (cdr rem) (car rem) res))
                     ((and (not (eqv? last-done #\g))
                           (or (peek? rem #\i)
                               (peek? rem #\e)
                               (peek? rem #\y)))
                      (loop (cdr rem) (car rem) (cons #\j res)))
                     (else
                       (loop (cdr rem) (car rem) (cons #\k res)))))
              ((#\h)
               (if (or (and (vowel? last-done) (not (peek? rem vowel?)))
                       (memv last-done '(#\c #\s #\p #\t #\g)))
                 (loop (cdr rem) (car rem) res)
                 (loop (cdr rem) (car rem) (cons #\h res))))
              ((#\k)
               (loop (cdr rem)
                     (car rem)
                     (if (eqv? last-done #\c)
                       res ; silent after c
                       (cons #\k res))))
              ((#\p)
               (loop (cdr rem)
                     (car rem)
                     (cons (if (peek? rem #\h) #\f #\p)
                           res)))
              ((#\q)
               (loop (cdr rem) (car rem) (cons #\k res)))
              ((#\s)
               (loop (cdr rem)
                     (car rem)
                     (cons (if (or (peek? rem #\h)
                                   (peek-io/a? rem))
                             #\x
                             #\s)
                           res)))
              ((#\t)
               (cond ((and (peek? rem #\c)        ; drop t if followed by ch
                           (peek? (cdr rem) #\h))
                      (loop (cdr rem) (car rem) res))
                     ((peek? rem #\h)             ; replace th with 0
                      (loop (cddr rem)
                            (car rem)
                            (cons #\0 res)))
                     ((peek-io/a? rem)            ; replace with x if followed by io or ia
                      (loop (cdr rem) (car rem) (cons #\x res)))
                     (else 
                       (loop (cdr rem) (car rem) (cons #\t res)))))
              ((#\v)
               (loop (cdr rem) (car rem) (cons #\f res)))
              ((#\w #\y)
               (loop (cdr rem)
                     (car rem)
                     (if (peek? rem vowel?)
                       (cons (car rem) res)
                       res)))
              ((#\x) ; TODO: x at start?
               (loop (cdr rem) (car rem) (cons #\s (cons #\k res))))
              ((#\z) 
               (loop (cdr rem) (car rem) (cons #\s res))))))))

    ; double-metaphone

    ;; Edit distance metrics
    ; damerau-levenshtein -- insertion/deletion/substitution/transposition
    ; jaro-distance       -- transposition
    ; longest-common-subsequence  -- insertion/deletion

    ; hamming-distance    -- substitution
    ; -- adapted to work on different sequence types
    (define hamming-distance 
      (case-lambda
        ((item-1 item-2)
         (hamming-distance item-1 item-2 (cond ((string? item-1) char=?)
                                               ((bytevector? item-1) =)
                                               (else equal?))))
        ((item-1 item-2 equal-test?)
         ;
         (define (hamming-seq-distance seq-1 seq-2 seq-length seq-for-each)
           (if (= (seq-length seq-1) (seq-length seq-2))
             (let ((count 0))
               (seq-for-each (lambda (c1 c2)
                               (unless (equal-test? c1 c2) (set! count (+ 1 count))))
                             seq-1
                             seq-2)
               count)
             (error "Hamming: Sequences are of different sizes")))
         (define (hamming-byte-distance vec-1 vec-2)
           (if (= (bytevector-length vec-1) (bytevector-length vec-2))
             (let loop ((count 0)
                        (i (bytevector-length vec-1)))
               (cond ((zero? i)
                      count)
                     ((= (bytevector-u8-ref vec-1 (- i 1))
                         (bytevector-u8-ref vec-2 (- i 1)))
                      (loop count
                            (- i 1)))
                     (else
                       (loop (+ 1 count)
                             (- i 1)))))
             (error "Hamming: Sequences are of different sizes")))
         ;
         (cond ((and (string? item-1) (string? item-2))
                (hamming-seq-distance item-1 item-2 string-length string-for-each))
               ((and (list? item-1) (list? item-2))
                (hamming-seq-distance item-1 item-2 length for-each))
               ((and (vector? item-1) (vector? item-2))
                (hamming-seq-distance item-1 item-2 vector-length vector-for-each))
               ((and (bytevector? item-1) (bytevector? item-2))
                (hamming-byte-distance item-1 item-2))
               (else
                 (error "Hamming: Unknown or mismatched types"))))))

    ;; internal function
    ;; depending on osa? flag, computes either levenshtein-distance or optimal-string-alignment-distance
    (define compute-seq-distance
      (case-lambda
        ((osa? item-1 item-2)
         (compute-seq-distance osa? item-1 item-2 (cond ((string? item-1) char=?)
                                                        ((bytevector? item-1) =)
                                                        (else equal?))))
        ((osa? item-1 item-2 equal-test?)
         (define (seq-distance seq-1 seq-2 seq-length seq-ref)
           (cond ((equal? seq-1 seq-2)
                  0)
                 ((zero? (seq-length seq-1))
                  (seq-length seq-2))
                 ((zero? (seq-length seq-2))
                  (seq-length seq-1))
                 (else
                   (let loop ((curr-2 (make-vector (+ 1 (seq-length seq-2)) 0))
                              (curr-1 (list->vector (list-tabulate (+ 1 (seq-length seq-2)) (lambda (x) x))))
                              (i 0))
                     (if (= i (seq-length seq-1)) ; finished
                       (vector-ref curr-1 (- (vector-length curr-1) 1))
                       (let ((curr (make-vector (vector-length curr-1) 0)))
                         ; compute current row values from the previous row
                         (vector-set! curr 0 (+ 1 i))
                         (do ((j 0 (+ 1 j)))
                           ((= j (seq-length seq-2)) )
                           (let ((cost (if (equal-test? (seq-ref seq-1 i) (seq-ref seq-2 j)) 0 1))) ; note 0/1 index
                             (vector-set! curr 
                                          (+ 1 j) 
                                          (min (+ 1 (vector-ref curr j))
                                               (+ 1 (vector-ref curr-1 (+ 1 j)))
                                               (+ cost (vector-ref curr-1 j))))
                             (when (and osa? ; extension for transpositions for optimal-string alignment 
                                        (> i 1)
                                        (> j 1)
                                        (equal-test? (seq-ref seq-1 i) (seq-ref seq-2 (- j 1)))
                                        (equal-test? (seq-ref seq-1 (- i 1)) (seq-ref seq-2 j)))
                               (vector-set! curr
                                            (+ 1 j)
                                            (min (vector-ref curr (+ 1 j))
                                                 (+ cost (vector-ref curr-2 (- j 1))))))))
                         (loop curr-1 curr (+ 1 i))))))))
         ;
         (cond ((and (string? item-1) (string? item-2))
                (seq-distance item-1 item-2 string-length string-ref))
               ((and (list? item-1) (list? item-2)) ; treat lists as vectors
                (seq-distance (list->vector item-1) (list->vector item-2) vector-length vector-ref))
               ((and (vector? item-1) (vector? item-2))
                (seq-distance item-1 item-2 vector-length vector-ref))
               ((and (bytevector? item-1) (bytevector? item-2))
                (seq-distance item-1 item-2 bytevector-length bytevector-u8-ref))
               (else
                 (error (if osa? 'optimal-string-alignment-distance 'levenshtein-distance) "Unknown or mismatched types"))))))


    ; levenshtein         -- insertion/deletion/substitution
    (define (levenshtein-distance . args)
      (apply compute-seq-distance (cons #f args)))

    ; optimal-string-alignment-distance -- insertion/deletion/substitution/transposition
    (define (optimal-string-alignment-distance . args)
      (apply compute-seq-distance (cons #t args)))

    ;; Character group measure: optional parameter for size of n-gram
    (define sorenson-dice-similarity 
      (case-lambda 
        ((string-1 string-2)
         (sorenson-dice-similarity string-1 string-2 2))
        ((string-1 string-2 n)
         (sorenson-dice-index (string->n-grams string-1 n) 
                              (string->n-grams string-2 n) 
                              string-ci=?))))

    ;; Porter stemming is a popular stemming algorithm used for English.
    ;;
    ;; For a complete description, see: <https://tartarus.org/martin/PorterStemmer/>
    ;;
    ;; Algorithm published in Porter, 1980, "An algorithm for suffix stripping," 
    ;; _Program_, Vol. 14, no. 3, pp 130-137.
    ;;
    ;; An error is raised if the word does not consist entirely of ASCII characters.
    ;; This is because the Porter stemmer is designed for stemming English words. 
    (define (porter-stem initial-word)
      ;
      ; Data structure holds the word and information on where we are in 
      ; processing it.
      ;
      ; b: array of character strings representing the word we are stemming
      ; j: an alternate 'end point' in the array, used, e.g., after searching 
      ;    to mark where a suffix begins
      ; k: the end of the array
      (define-record-type <data>
        (new-data b j k)
        data?
        (b b-get)
        (j j-get j-set!)
        (k k-get k-set!))
      ;
      ; checks if there is a double consonant at position i,
      ; with i being the second consonant's index
      (define (double-consonant? data i)
        (and (> i 0)
             (char=? (list-ref (b-get data) i)
                     (list-ref (b-get data) (- i 1)))
             (is-consonant? data i)))
      ;
      ; returns true if the suffix is found at the end of the current
      ; set of bytes, with k holding the last index in bytes.
      ; sets j to the end point, as if the suffix is removed.
      (define (ends-with? data suffix)
        (let ((suffix-array (string->list suffix))
              (suffix-length (string-length suffix)))
          (if (and (<= suffix-length (k-get data))
                   (equal? (take-right (take (b-get data) (+ 1 (k-get data))) suffix-length)
                           suffix-array))
            (begin
              (j-set! data (- (k-get data) suffix-length))
              #t)
            #f)))
      ;
      ; Checks if there is a consonant at position i
      ; -- note special handling of 'y', which is treated
      ; as a consonant at start of word or after a vowel.
      (define (is-consonant? data i)
        (cond ((member (list-ref (b-get data) i)
                       '(#\a #\e #\i #\o #\u))
               #f)
              ((char=? (list-ref (b-get data) i) 
                       #\y)
               (or (= i 0)
                   (not (is-consonant? data (- i 1)))))
              (else
                #t)))
      ;
      ; cvc is true if i-2, i-1, i has form consonant, vowel, consonant
      ; (and last must not be w, x, or y)
      (define (is-cvc? data i)
        (cond ((or (< i 2)
                   (not (is-consonant? data i))
                   (is-consonant? data (- i 1))
                   (not (is-consonant? data (- i 2))))
               #f)
              ((let ((ch (list-ref (b-get data) i)))
                 (or (char=? ch #\w)
                     (char=? ch #\x)
                     (char=? ch #\y)))
               #f)
              (else 
                #t)))
      ;
      ; Given a set of letter=>[(suffix, replacement)] pairs and a letter to match,
      ; applies replacements for the given letter
      (define (make-replacements data letter replacements)
        (for-each (lambda (rule)
                    (when (char=? letter (car rule))
                      (for-each (lambda (pairing) 
                                  (when (ends-with? data (car pairing))
                                    (set-to data (cadr pairing) #t)))
                                (cdr rule))))
                  replacements))
      ;
      ; counts the number of vowel-consonant sequences between 0 and j
      (define (seq-count data)
        (let ((count 0)
              (i 0))
          ; find index of first vowel, by skipping consonants
          (let loop ()
            (when (and (<= i (j-get data))
                       (is-consonant? data i))
              (set! i (+ 1 i))
              (loop)))
          ; repeat CV search to get full count until all of range is seen
          (let loop ()
            (when (<= i (j-get data))
              ; find index of next consonant, by skipping non-consonants
              (let loop-consonant ()
                (when (and (<= i (j-get data))
                           (not (is-consonant? data i)))
                  (set! i (+ 1 i))
                  (loop-consonant)))
              ; VC pair found with more to come, so increment count
              (when (<= i (j-get data))
                (set! count (+ 1 count)))
              ; find index of next vowel, by skipping consonants
              (let loop-vowel ()
                (when (and (<= i (j-get data))
                           (is-consonant? data i))
                  (set! i (+ 1 i))
                  (loop-vowel)))
              (loop)))
          ; 
          count))
      ;
      ; sets positions j+1 to k to the suffix,
      ; adjusting k to j+length(suffix) (the new end of the word)
      ; -- supports a flag for only allowing change if seq_count > 0
      ; this prevents words becoming too small when suffix changed
      (define set-to
        (case-lambda 
          ((data suffix)
           (set-to data suffix #f))
          ((data suffix restrict)
           (when (or (not restrict)
                     (> (seq-count data) 0))
             (for-each (lambda (index value)
                         (list-set! (b-get data) (+ 1 (j-get data) index) value))
                       (iota (string-length suffix))
                       (string->list suffix))
             (k-set! data (+ (j-get data)
                             (string-length suffix)))))))
      ;
      ; looks for a vowel in range 0 to j in bytes
      (define (vowel-in-stem? data)
        (any (lambda (i) (not (is-consonant? data i)))
             (iota (+ 1 (j-get data)))))
      ;
      (define STEP-2-RULES (list (cons #\a '(("ational" "ate") ("tional" "tion")))
                                 (cons #\c '(("enci" "ence") ("anci" "ance")))
                                 (cons #\e '(("izer" "ize")))
                                 (cons #\g '(("logi" "log")))
                                 (cons #\l '(("bli" "ble") ("alli" "al") ("entli" "ent") ("eli" "e") ("ousli" "ous")))
                                 (cons #\o '(("ization" "ize") ("ation" "ate") ("ator" "ate")))
                                 (cons #\s '(("alism" "al") ("iveness" "ive") ("fulness" "ful") ("ousness" "ous")))
                                 (cons #\t '(("aliti" "al") ("iviti" "ive") ("biliti" "ble")))))
      ;
      (define STEP-3-RULES (list (cons #\e '(("icate" "ic") ("ative" "") ("alize" "al")))
                                 (cons #\i '(("iciti" "ic")))
                                 (cons #\l '(("ical" "ic") ("ful" "")))
                                 (cons #\s '(("ness" "")))))
      ;
      ; removes plurals and -ed -ing endings
      (define (step-1ab data)
        (when (char=? (list-ref (b-get data) (k-get data)) #\s)
          (cond ((ends-with? data "sses")
                 (k-set! data (- (k-get data) 2)))
                ((ends-with? data "ies")
                 (set-to data "i"))
                ((not (char=? (list-ref (b-get data) (- (k-get data) 1))
                              #\s))
                 (k-set! data (- (k-get data) 1)))))
        (if (ends-with? data "eed")
          (when (> (seq-count data) 0)
            (k-set! data (- (k-get data) 1)))
          (when (and (or (ends-with? data "ed")
                         (ends-with? data "ing"))
                     (vowel-in-stem? data))
            (k-set! data (j-get data))
            (cond ((ends-with? data "at")
                   (set-to data "ate"))
                  ((ends-with? data "bl")
                   (set-to data "ble"))
                  ((ends-with? data "iz")
                   (set-to data "ize"))
                  ((double-consonant? data (k-get data))
                   (k-set! data (- (k-get data) 1))
                   (let ((ch (list-ref (b-get data) (k-get data))))
                     (when (or (char=? #\l ch)
                               (char=? #\s ch)
                               (char=? #\z ch))
                       (k-set! data (+ (k-get data) 1)))))
                  ((and (= 1 (seq-count data))
                        (is-cvc? data (k-get data)))
                   (set-to data "e"))))))
      ; 
      ; turns terminal 'y' to 'i' when there is another vowel in stem
      (define (step-1c data)
        (when (and (ends-with? data "y")
                   (vowel-in-stem? data))
          (list-set! (b-get data) (k-get data) #\i)))
      ;
      ; double suffixes are mapped to single (shorter) ones
      (define (step-2 data)
        (make-replacements data 
                           (list-ref (b-get data) (- (k-get data) 1))
                           STEP-2-RULES))
      ;
      ; deals with -ic -full -ness
      (define (step-3 data)
        (make-replacements data 
                           (list-ref (b-get data) (k-get data))
                           STEP-3-RULES))
      ;
      ; removes -ant -ence when seq-count is 2
      (define (step-4 data)
        (call-with-current-continuation
          (lambda (return)
            (let ((letter (list-ref (b-get data) (- (k-get data) 1)))) ; penultimate letter
              (cond ((char=? letter #\a)
                     (when (not (ends-with? data "al"))
                       (return)))
                    ((char=? letter #\c)
                     (when (and (not (ends-with? data "ance"))
                                (not (ends-with? data "ence")))
                       (return)))
                    ((char=? letter #\e)
                     (when (not (ends-with? data "er"))
                       (return)))
                    ((char=? letter #\i)
                     (when (not (ends-with? data "ic"))
                       (return)))
                    ((char=? letter #\l)
                     (when (and (not (ends-with? data "able"))
                                (not (ends-with? data "ible")))
                       (return)))
                    ((char=? letter #\n)
                     (when (and (not (ends-with? data "ant"))
                                (not (ends-with? data "ement"))
                                (not (ends-with? data "ment"))
                                (not (ends-with? data "ent")))
                       (return)))
                    ((char=? letter #\o)
                     (if (and (ends-with? data "ion")
                              (or (char=? (list-ref (b-get data) (j-get data)) #\s)
                                  (char=? (list-ref (b-get data) (j-get data)) #\t)))
                       '() ; break
                       (when (not (ends-with? data "ou"))
                         (return))))
                    ((char=? letter #\s)
                     (when (not (ends-with? data "ism"))
                       (return)))
                    ((char=? letter #\t)
                     (when (and (not (ends-with? data "ate"))
                                (not (ends-with? data "iti")))
                       (return)))
                    ((char=? letter #\u)
                     (when (not (ends-with? data "ous"))
                       (return)))
                    ((char=? letter #\v)
                     (when (not (ends-with? data "ive"))
                       (return)))
                    ((char=? letter #\z)
                     (when (not (ends-with? data "ize"))
                       (return)))
                    (else
                      (return)))
              (when (> (seq-count data) 1)
                (k-set! data (j-get data)))))))
      ;
      ; if seq-count > 1, removes a final -e and changes -ll to -l
      (define (step-5 data)
        (j-set! data (k-get data))
        ; remove final -e
        (when (char=? (list-ref (b-get data) (k-get data))
                      #\e)
          (let ((count (seq-count data)))
            (when (or (> count 1)
                      (and (= count 1)
                           (not (is-cvc? data (- (k-get data) 1)))))
              (k-set! data (- (k-get data) 1)))))
        ; -ll to -l
        (when (and (char=? (list-ref (b-get data) (k-get data))
                           #\l)
                   (double-consonant? data (k-get data))
                   (> (seq-count data) 1))
          (k-set! data (- (k-get data) 1))))
      ;
      ; - main part of function
      ;
      (let ((word (string-downcase initial-word))) ; TODO - check ASCII
        (if (< (string-length word) 3) ; do not process short words
          word
          (let ((data (new-data (string->list word) 0 (- (string-length word) 1))))
            (step-1ab data)
            (when (> (k-get data) 0)
              (step-1c data)
              (step-2 data)
              (step-3 data)
              (step-4 data)
              (step-5 data))
            (list->string (take (b-get data) (+ 1 (k-get data))))))))

    ;; password-generator
    ;; returns two values - the generated password, and its entropy
    ;; error if options prohibit construction of a password
    (define generate-password
      (case-lambda
        ((size)
         (generate-password size #t #t #t #t #t))
        ((size allow-duplicates? use-lowercase? use-numbers? use-symbols? use-uppercase?)
         (unless (and (integer? size) (positive? size))
           (error "Given size must be a positive integer." ))
         (let ((available-characters 
                 (append 
                   (if use-lowercase? '(#\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m #\n #\o #\p #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z) '())
                   (if use-numbers? '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9) '())
                   (if use-symbols? '(#\! #\" #\' #\# #\$ #\% #\& #\( #\) #\* #\+ #\, #\- #\. #\/ #\: #\; #\< #\= #\> #\? #\` #\~ #\{ #\| #\} #\@ #\^) '())
                   (if use-uppercase? '(#\A #\B #\C #\D #\E #\F #\G #\H #\I #\J #\K #\L #\M #\N #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X #\Y #\Z) '()))))
           (cond ((null? available-characters)
                  (error "Selection resulted in no available characters."))
                 ((and (not allow-duplicates?)
                       (> size (length available-characters)))
                  (error "No duplicates is selected, but the required password size exceeds the number of available characters."))
                 (else
                   (let loop ((selected-characters '()))
                     (if (= size (length selected-characters))
                       (values (list->string selected-characters)
                               (* size (log (length available-characters) 2)))
                       (let ((rnd-char (random-pick available-characters)))
                         (if (or allow-duplicates?
                                 (not (member rnd-char selected-characters)))
                           (loop (cons rnd-char selected-characters))
                           (loop selected-characters)))))))))))

    ))

