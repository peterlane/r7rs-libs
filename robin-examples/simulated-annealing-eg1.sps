;; Example of Simulated Annealing

(import (scheme base)
        (scheme case-lambda)
        (scheme list-queue)
        (scheme write)
        (srfi 27)
        (slib charplot)
        (robin simulated-annealing))

(define make-instance
  (case-lambda 
    (()
     (* (random-real) 10.0))
    ((x)
     (let ((new-x (+ x (* 0.02 (random-real)) -0.01)))
       (cond ((< new-x 0)
              0.0)
             ((> new-x 10)
              10.0)
             (else
               new-x))))))

(define history (list-queue))

(define (evaluate-instance x)
  (let ((result (let ((y (+ (* x x) (* -7 x) 10)))
                  (square y))))
    (list-queue-add-back! history result)
    result))

(let ((result (simulated-annealing make-instance evaluate-instance 10000)))
  (display "Final value of x: ") (display result) (newline))

(charplot:dimensions '(20 60))
(plot (let ((pts '()))
        (list-queue-for-each (lambda (pt) 
                               (set! pts (cons (list (length pts) pt) pts)))
                             history)
        pts)
      "cycle number"
      "evaluation") 

