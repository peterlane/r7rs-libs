(import (scheme base)
        (scheme write)
        (robin confusion-matrix))

(define cm (make-confusion-matrix (list 'pos 'neg)))

(confusion-matrix-add cm 'pos 'pos 10)
(confusion-matrix-add cm 'pos 'neg 3)
(confusion-matrix-add cm 'neg 'neg 20)
(confusion-matrix-add cm 'neg 'pos 5)

(display "Confusion Matrix") (newline) (newline)
(confusion-matrix-display cm)

(display "Precision: ") (display (inexact (precision cm))) (newline)
(display "Recall: ") (display (inexact (recall cm))) (newline)
(display "Matthews-Correlation: ") (display (inexact (matthews-correlation cm))) (newline)

