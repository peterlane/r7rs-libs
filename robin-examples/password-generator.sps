;; Password generator

(import (scheme base)
        (scheme char)
        (scheme inexact)
        (scheme process-context)
        (scheme show)
        (robin text))

(define (help-message)
  (show #t "password-generator: [flags] size" nl) 
  (show #t "  d  allow-duplicates" nl) 
  (show #t "  l  use lowercase letters" nl) 
  (show #t "  n  use numbers" nl) 
  (show #t "  s  use symbols" nl) 
  (show #t "  u  use uppercase letters" nl) 
  (show #t nl "Not specifying any flags will use all groups with duplicates." nl))

(define (generate-password-with-args size-arg flags-arg)
  (let* ((size (string->number size-arg))
         (flags (string->list (string-downcase flags-arg)))
         (allow-duplicates? (or (null? flags)
                                (member #\d flags)))
         (use-lowercase? (or (null? flags)
                             (member #\l flags)))
         (use-numbers? (or (null? flags)
                           (member #\n flags)))
         (use-symbols? (or (null? flags)
                           (member #\s flags)))
         (use-uppercase? (or (null? flags)
                             (member #\u flags))))
    ; 
    (guard (condition
             ((error-object? condition)
              (show #t "Error: " (error-object-message condition) nl)
              (help-message)))
      (let-values (((password entropy) (generate-password size 
                                                          allow-duplicates? 
                                                          use-lowercase? 
                                                          use-numbers? 
                                                          use-symbols? 
                                                          use-uppercase?)))
        (show #t "Password is: " nl password nl "with entropy: " entropy nl)))))

;; get options from command line
;; - [flags] number
(let ((arguments (command-line)))
  (case (length arguments)
    ((2)
     (generate-password-with-args (list-ref arguments 1)
                                  ""))
    ((3)
     (generate-password-with-args (list-ref arguments 2)
                                  (list-ref arguments 1)))
    (else
      (help-message))))

