
(import (scheme base)
        (robin confusion-matrix)
        (srfi 64))

(test-begin "robin-confusion-matrix")

;; empty case
(let ((cm (make-confusion-matrix))) ; use default classes
  (test-assert (zero? (confusion-matrix-total cm)))
  (test-assert (zero? (confusion-matrix-count cm 'positive 'positive)))
  (test-assert (zero? (confusion-matrix-count cm 'positive 'negative)))
  (test-assert (zero? (confusion-matrix-count cm 'negative 'positive)))
  (test-assert (zero? (confusion-matrix-count cm 'negative 'negative))))

;; two classes
(let ((cm (make-confusion-matrix))) ; use default classes
  (do ((i 0 (+ 1 i)))
    ((= i 10))
    (confusion-matrix-add cm 'positive 'positive)) ; try with default
  (confusion-matrix-add cm 'positive 'negative 5)
  (do ((i 0 (+ 1 i)))
    ((= i 20))
    (confusion-matrix-add cm 'negative 'negative)) ; try with default
  (confusion-matrix-add cm 'negative 'positive 5)

  (test-equal (list 'positive 'negative) (confusion-matrix-labels cm))
  (test-equal 10 (confusion-matrix-count cm 'positive 'positive))
  (test-equal 5 (confusion-matrix-count cm 'positive 'negative))
  (test-equal 20 (confusion-matrix-count cm 'negative 'negative))
  (test-equal 5 (confusion-matrix-count cm 'negative 'positive))

  (test-equal 40 (confusion-matrix-total cm))
  (test-equal 10 (true-positive cm))
  (test-equal 5 (false-negative cm))
  (test-equal 20 (true-negative cm))
  (test-equal 5 (false-positive cm))
  (test-equal 20 (true-positive cm 'negative))
  (test-equal 5 (false-negative cm 'negative))
  (test-equal 10 (true-negative cm 'negative))
  (test-equal 5 (false-positive cm 'negative))

  (test-approximate 0.6667 (true-rate cm) 0.001)
  (test-approximate 0.8 (true-rate cm 'negative) 0.001)
  (test-approximate 0.2 (false-rate cm) 0.001)
  (test-approximate 0.3333 (false-rate cm 'negative) 0.001)
  (test-approximate 0.6667 (precision cm) 0.001)
  (test-approximate 0.8 (precision cm 'negative) 0.001)
  (test-approximate 0.6667 (recall cm) 0.001)
  (test-approximate 0.8 (recall cm 'negative) 0.001)
  (test-approximate 0.6667 (sensitivity cm) 0.001)
  (test-approximate 0.8 (sensitivity cm 'negative) 0.001)
  (test-approximate 0.75 (overall-accuracy cm) 0.001)
  (test-approximate 0.6667 (f-measure cm) 0.001)
  (test-approximate 0.8 (f-measure cm 'negative) 0.001)
  (test-approximate 0.7303 (geometric-mean cm) 0.001)
  )

;; two-classes 2
;; -- Example from: 
;;    https://www.datatechnotes.com/2019/02/accuracy-metrics-in-classification.html
(let ((cm (make-confusion-matrix))) ; use default classes
  (confusion-matrix-add cm 'positive 'positive 5)
  (confusion-matrix-add cm 'positive 'negative 1)
  (confusion-matrix-add cm 'negative 'negative 3)
  (confusion-matrix-add cm 'negative 'positive 2)

  (test-equal 11 (confusion-matrix-total cm))
  (test-equal 5 (true-positive cm))
  (test-equal 1 (false-negative cm))
  (test-equal 2 (false-positive cm))
  (test-equal 3 (true-negative cm))

  (test-approximate 0.7142 (precision cm) 0.001)
  (test-approximate 0.8333 (recall cm) 0.001)
  (test-approximate 0.7272 (overall-accuracy cm) 0.001)
  (test-approximate 0.7692 (f-measure cm) 0.001)
  (test-approximate 0.8333 (sensitivity cm) 0.001)
  (test-approximate 0.6 (specificity cm) 0.001)
  (test-approximate 0.4407 (cohen-kappa cm) 0.001)
  (test-approximate 0.5454 (prevalence cm) 0.001)
  )

;; Examples from:
;; https://standardwisdom.com/softwarejournal/2011/12/matthews-correlation-coefficient-how-well-does-it-do/
(define (test-case a b c d e f g h i)
  (let ((cm (make-confusion-matrix)))
    (confusion-matrix-add cm 'positive 'positive a)
    (confusion-matrix-add cm 'positive 'negative b)
    (confusion-matrix-add cm 'negative 'negative c)
    (confusion-matrix-add cm 'negative 'positive d)

    (test-approximate e (matthews-correlation cm) 0.001)
    (test-approximate f (precision cm) 0.001)
    (test-approximate g (recall cm) 0.001)
    (test-approximate h (f-measure cm) 0.001)
    (test-approximate i (cohen-kappa cm) 0.001)))

(test-case 100 0 900 0 1.0 1.0 1.0 1.0 1.0)
(test-case 65 35 825 75 0.490 0.4643 0.65 0.542 0.4811)
(test-case 50 50 700 200 0.192 0.2 0.5 0.286 0.1666)

;; three classes
(let ((cm (make-confusion-matrix '(red blue green))))
  (confusion-matrix-add cm 'red 'red 10)
  (confusion-matrix-add cm 'red 'blue 7)
  (confusion-matrix-add cm 'red 'green 5)
  (confusion-matrix-add cm 'blue 'red 20)
  (confusion-matrix-add cm 'blue 'blue 5)
  (confusion-matrix-add cm 'blue 'green 15)
  (confusion-matrix-add cm 'green 'red 30)
  (confusion-matrix-add cm 'green 'blue 12)
  (confusion-matrix-add cm 'green 'green 8)

  (test-equal 112 (confusion-matrix-total cm))
  (test-equal 10 (true-positive cm))
  (test-equal 12 (false-negative cm))
  (test-equal 50 (false-positive cm))
  (test-equal 13 (true-negative cm))
  (test-equal 5 (true-positive cm 'blue))
  (test-equal 35 (false-negative cm 'blue))
  (test-equal 19 (false-positive cm 'blue))
  (test-equal 18 (true-negative cm 'blue))
  (test-equal 8 (true-positive cm 'green))
  (test-equal 42 (false-negative cm 'green))
  (test-equal 20 (false-positive cm 'green))
  (test-equal 15 (true-negative cm 'green))
  )

(test-end)

