;; SRFI 27 Random Bits
;; implementation for Kawa, based around java.util.Random

(define-library 
  (srfi 27)
  (export random-integer
          random-real
          default-random-source
          make-random-source
          random-source?
          random-source-state-ref
          random-source-state-set!
          random-source-randomize!
          random-source-pseudo-randomize!
          random-source-make-integers
          random-source-make-reals)
  (import (scheme base)
          (class java.lang
                 Integer Long System)
          (class java.util.concurrent.atomic
                 AtomicLong)
          (class java.lang.reflect
                 Field)
          (class java.math
                 BigInteger)
          (class java.util
                 Random)
          (only (kawa base) as invoke logxor static-field))

  (begin

    ;; Java's Random class does not provide a useful way to get the seed,
    ;; so we will attempt to store the current seed value
    ;; NB: this may not robust, so random-source-state-ref may not be correct.
    (define *current-seed* 0)

    ;; -- private method

    (define MAX-INTEGER (static-field java.lang.Integer 'MAX_VALUE))
    (define MAX-LONG (static-field java.lang.Long 'MAX_VALUE))

    ;; Uses BigInteger to permit arbitrarily large n
    (define (random-integer-from-source source n)
      (if (<= n MAX-INTEGER)
        (invoke source 'nextInt n)
        (let* ((input (BigInteger (number->string n)))
               (bits (invoke input 'bitLength)))
          (do ((r (BigInteger bits (as Random source))
                  (BigInteger bits (as Random source))))
            ((< r input) r)))))

    ;; -- exported methods

    (define default-random-source (make-random-source))

    (define (random-integer n)
      (random-integer-from-source default-random-source n))

    (define (random-real)
      (invoke default-random-source 'nextFloat))

    (define (make-random-source)
      (Random *current-seed*))

    (define (random-source? source)
      (Random? source))

    (define (random-source-state-ref source)
      *current-seed*)

    (define (random-source-state-set! source seed)
      (set! *current-seed* seed)
      (invoke source 'setSeed *current-seed*))

    (define (random-source-randomize! source)
      (random-source-state-set! source (invoke System 'currentTimeMillis)))

    ;; constructs a long from i j in a simple but deterministic manner
    (define (random-source-pseudo-randomize! source i j)
      (random-source-state-set! source 
                                (modulo (+ i (* j (+ MAX-INTEGER 1))) 
                                        MAX-LONG)))

    (define (random-source-make-integers source)
      (lambda (n) (random-integer-from-source source n)))

    (define (random-source-make-reals source)
      (lambda () (invoke source 'nextFloat)))

    ))



