;; SRFI 14 reference implementation packaged for R7RS Scheme

;;; SRFI-14 character-sets library				-*- Scheme -*-
;;;
;;; - Ported from MIT Scheme runtime by Brian D. Carlstrom.
;;; - Massively rehacked & extended by Olin Shivers 6/98.
;;; - Massively redesigned and rehacked 5/2000 during SRFI process.
;;; At this point, the code bears the following relationship to the
;;; MIT Scheme code: "This is my grandfather's axe. My father replaced
;;; the head, and I have replaced the handle." Nonetheless, we preserve
;;; the MIT Scheme copyright:
;;;     Copyright (c) 1988-1995 Massachusetts Institute of Technology
;;; The MIT Scheme license is a "free software" license. See the end of
;;; this file for the tedious details. 

(define-library
  (scheme charset)
  (export
    ; char-set? - TODO - how to export in Kawa?
    char-set= char-set<=
    char-set-hash 
    char-set-cursor char-set-ref char-set-cursor-next end-of-char-set?
    char-set-fold char-set-unfold char-set-unfold!
    char-set-for-each char-set-map
    char-set-copy char-set

    list->char-set  string->char-set 
    list->char-set! string->char-set! 

    char-set-filter  ucs-range->char-set  ->char-set
    char-set-filter! ucs-range->char-set!

    char-set->list char-set->string

    char-set-size char-set-count char-set-contains?
    char-set-every char-set-any

    char-set-adjoin  char-set-delete 
    char-set-adjoin! char-set-delete!


    char-set-complement  char-set-union  char-set-intersection  
    char-set-complement! char-set-union! char-set-intersection! 

    char-set-difference  char-set-xor  char-set-diff+intersection
    char-set-difference! char-set-xor! char-set-diff+intersection!

    char-set:lower-case		char-set:upper-case	char-set:title-case
    char-set:letter		char-set:digit		char-set:letter+digit
    char-set:graphic		char-set:printing	char-set:whitespace
    char-set:iso-control	char-set:punctuation	char-set:symbol
    char-set:hex-digit		char-set:blank		char-set:ascii
    char-set:empty		char-set:full
    )
  (import (srfi 14)))

