echo off

for %%f in (scheme-tests/*-test.sps) do (
  call kawa --r7rs "scheme-tests/%%~nf.sps"
)

for %%f in (srfi-tests/*-test.sps) do (
  call kawa --r7rs "srfi-tests/%%~nf.sps"
)

cd ..

for %%f in (nltk-tests/*-test.sps) do (
  call kawa --r7rs "nltk-tests/%%~nf.sps"
)

for %%f in (pfds-tests/*-test.sps) do (
  call kawa --r7rs "pfds-tests/%%~nf.sps"
)

for %%f in (rebottled-tests/*-test.sps) do (
  call kawa --r7rs "rebottled-tests/%%~nf.sps"
)

for %%f in (robin-tests/*-test.sps) do (
  call kawa --r7rs "robin-tests/%%~nf.sps"
)


for %%f in (slib-tests/*-test.sps) do (
  call kawa --r7rs "slib-tests/%%~nf.sps"
)

cd kawa
