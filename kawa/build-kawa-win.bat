:: Note, unless you preset bin on your CLASSPATH
:: This will produce 'missing class' errors as you compile on windows

for %%f in (srfi/*.sld) do (
  echo %%f
  call kawa -d bin --r7rs -C "srfi/%%~nf.sld"
)

for %%f in (scheme/*.sld) do (
  echo %%f
  call kawa -d bin --r7rs -C "scheme/%%~nf.sld"
)

cd ..
for %%f in (nltk/*.sld) do (
  echo %%f
  call kawa -d kawa/bin --r7rs -C "nltk/%%~nf.sld"
)
for %%f in (pfds/*.sld) do (
  echo %%f
  call kawa -d kawa/bin --r7rs -C "pfds/%%~nf.sld"
)
for %%f in (rebottled/*.sld) do (
  echo %%f
  call kawa -d kawa/bin --r7rs -C "rebottled/%%~nf.sld"
)
for %%f in (robin/*.sld) do (
  echo %%f
  call kawa -d kawa/bin --r7rs -C "robin/%%~nf.sld"
)
for %%f in (slib/*.sld) do (
  echo %%f
  call kawa -d kawa/bin --r7rs -C "slib/%%~nf.sld"
)
for %%f in (srfi/*.sld) do (
  echo %%f
  call kawa -d kawa/bin --r7rs -C "srfi/%%~nf.sld"
)

cd kawa

cd bin
jar cf r7rs-libs.jar .
cd ..
move bin\r7rs-libs.jar .

